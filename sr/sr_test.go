package sr

import (
	"fmt"
	"os"

	"github.com/ctessum/geom/carto"
	"github.com/gonum/floats"
	"github.com/gonum/plot/vg"
	"github.com/gonum/plot/vg/draw"
	"github.com/gonum/plot/vg/vgimg"
)

func Example() {
	// This example opens the SR matrix file, retrieves the changes in
	// concentrations caused by emissions at the center of the SR
	// spatial domain, weights the concentration changes by population,
	// and finally creates a map of population-weighted concentrations
	// in cells with 500 km of the source.

	// Open sr file
	f, err := os.Open("sr.ncf")
	if err != nil {
		panic(err)
	}
	// create sr object from file
	data, err := New(f)
	if err != nil {
		panic(err)
	}
	// load population
	pop, err := data.Population("TotalPop")
	if err != nil {
		panic(err)
	}
	// load grid cells.
	cells, err := data.GridCells()
	if err != nil {
		panic(err)
	}
	const (
		x         = 0      // meters; center of domain
		y         = 0      // meters; center of domain
		r         = 500000 // meters
		layer     = 2      // elevated release
		pollutant = "pso4" // emissions of SOx causing pso4 concentrations
	)
	sourceIndex := cells.Index(x, y)
	cellsOfInterest := cells.IndicesWithinRadius(x, y, r)

	conc, err := data.Source(pollutant, sourceIndex, layer)
	if err != nil {
		panic(err)
	}

	// calculate population weighted concentrations
	popSum := floats.Sum(pop)
	for i, c := range conc {
		conc[i] = c * pop[i] / popSum
	}

	// create color map.
	cmap := carto.NewColorMap(carto.Linear)
	cmap.AddArray(conc)
	cmap.Set()

	const (
		figWidth     = 4 * vg.Inch
		figHeight    = 3.1 * vg.Inch
		legendHeight = 0.3 * vg.Inch
	)

	// create image
	ic := vgimg.New(figWidth, figHeight)
	dc := draw.New(ic)
	legendc := draw.Crop(dc, 0, 0, 0, legendHeight-figHeight)
	plotc := draw.Crop(dc, 0, 0, legendHeight, 0)

	// create legend
	cmap.Legend(&legendc, "Population-weighted concentration (μg/m3/ton)")

	// create map
	c := carto.NewCanvas(600000, -600000, 600000, -600000, plotc)
	for _, i := range cellsOfInterest {
		// only plot cells within 500,000 m of source.
		color := cmap.GetColor(conc[i])
		ls := draw.LineStyle{
			Color: color,
			Width: vg.Points(0.5),
		}
		err = c.DrawVector(cells.Cells[i], color, ls, draw.GlyphStyle{})
		if err != nil {
			panic(err)
		}
	}

	// save image to file
	w, err := os.Create("sr_example.png")
	if err != nil {
		panic(err)
	}
	_, err = vgimg.PngCanvas{Canvas: ic}.WriteTo(w)
	if err != nil {
		panic(err)
	}

	fmt.Println("Output image can be viewed at https://bytebucket.org/ctessum/sr/raw/master/sr/sr_example.png")
	// Output:
	// Output image can be viewed at https://bytebucket.org/ctessum/sr/raw/master/sr/sr_example.png
}
