This library contains functions for dealing with a source-receptor matrix formatted as a NetCDF database.

The netcdf file is formatted as described below. 

There are three emissions heights (the "layer" dimension) that represent emissions height ranges of 0-57, 57-140, and 760-1000 m, respectively.

When the SR matrix was being created, at around source number 29878 the version of InMAP being used was switched from a version with 50108 grid cells to a version of 50105 grid cells. In the new version, cells 8611, 8612, 8613, and 8614 were combined into a single grid cell. In the NetCDF file, this issue has already been adjusted for in the receptors associated with each source, but it has not been adjusted for in the sources. The software library that this README accompanies contains an adjustment for this issue which can be used as a guide for users that do not wish to use the Go language to access the data.


netcdf sr {
dimensions:
	layer = 3 ; // number of emissions heights
	source = 50105 ; // number of grid cells
	receptor = 50105 ; // number of grid cells
variables:
	float soa(layer, source, receptor) ; // Annual average μg/m3 of SOA formed from 1 ton/year VOC emissions.
	float primarypm2_5(layer, source, receptor) ;// Annual average PM2.5 caused by from 1 ton/year primary PM2.5 emissions.
	float pnh4(layer, source, receptor) ;// Annual average μg/m3 of particulate NH4 formed from 1 ton/year NH3 emissions.
	float pso4(layer, source, receptor) ;// Annual average μg/m3 of particulate SO4 formed from 1 ton/year SO2 emissions.
	float pno3(layer, source, receptor) ;// Annual average μg/m3 of particulate NO3 formed from 1 ton/year NOx emissions.
	float N(source) ; // Northern extent of each grid cell.
	float S(source) ; // Southern extent of each grid cell.
	float E(source) ; // Eastern extent of each grid cell.
	float W(source) ; // Western extent of each grid cell.
	float TotalPop(source) ; // Total population in each grid cell.
	float WhiteNoLat(source) ; // White non-latino population in each grid cell
	float Black(source) ; // Black population in each grid cell.
	float Native(source) ;// Native population in each grid cell.
	float Asian(source) ;// Asian population in each grid cell.
	float Other(source) ;// Other population in each grid cell.
	float Latino(source) ;// Latino population in each grid cell.
	float Poverty(source) ;	// Below-poverty-level population in each grid cell.
	float TwoXPov(source) ;// Population in each grid cell at at least twice the poverty level.

	float AllCause(source) ; // Baseline mortality rate per 100,000 per year.
}
