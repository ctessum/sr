package sr

import (
	"fmt"
	"math"

	"bitbucket.org/ctessum/cdf"
	"github.com/ctessum/geom"
	"github.com/ctessum/geom/index/rtree"
	"github.com/ctessum/geom/op"
	"github.com/gonum/floats"
)

const numCells int = 50105

// SR holds information about a NetCDF-formatted source-receptor (SR) database.
type SR struct {
	cdf.File
}

// New creates a new SR from the netcdf database specified by r.
func New(r cdf.ReaderWriterAt) (*SR, error) {
	cf, err := cdf.Open(r)
	return &SR{
		File: *cf,
	}, err
}

// Population returns the population in each grid cell of the population
// type specified by popType. Current options for popType are
// "TotalPop", "WhiteNoLat", "Black", "Native", "Asian", "Other", "Latino",
// "Poverty", and "TwoXPov".
func (sr *SR) Population(popType string) ([]float64, error) {
	popTypes := []string{"TotalPop", "WhiteNoLat",
		"Black", "Native", "Asian", "Other", "Latino",
		"Poverty", "TwoXPov"}

	found := false
	for _, p := range popTypes {
		if popType == p {
			found = true
			break
		}
	}
	if !found {
		return nil, fmt.Errorf("in sr.Population, %s is not a valid popType",
			popType)
	}
	return sr.readFullVar(popType)
}

// MortalityBaseline returns the baseline mortality rate in deaths/100,000/year.
func (sr *SR) MortalityBaseline() ([]float64, error) {
	return sr.readFullVar("AllCause")
}

// GridCells holds information on the Geometry of the SR grid cells.
type GridCells struct {
	Cells []geom.Polygonal
	tree  *rtree.Rtree
	geom.Bounds
}

type cellIndex struct {
	geom.Polygonal
	index int
}

// GridCells returns the geometry of the InMAP grid cells.
func (sr *SR) GridCells() (*GridCells, error) {
	n, err := sr.readFullVar("N")
	if err != nil {
		return nil, err
	}
	s, err := sr.readFullVar("S")
	if err != nil {
		return nil, err
	}
	e, err := sr.readFullVar("E")
	if err != nil {
		return nil, err
	}
	w, err := sr.readFullVar("W")
	if err != nil {
		return nil, err
	}
	t := rtree.NewTree(25, 50)
	c := make([]geom.Polygonal, len(n))
	for i := range n {
		cc := geom.Polygon{
			[]geom.Point{
				geom.Point{X: w[i], Y: s[i]},
				geom.Point{X: e[i], Y: s[i]},
				geom.Point{X: e[i], Y: n[i]},
				geom.Point{X: w[i], Y: n[i]},
				geom.Point{X: w[i], Y: s[i]},
			},
		}
		c[i] = cc
		t.Insert(cellIndex{Polygonal: cc, index: i})
	}
	return &GridCells{
		Cells: c,
		tree:  t,
		Bounds: geom.Bounds{
			Min: geom.Point{X: floats.Min(w), Y: floats.Min(s)},
			Max: geom.Point{X: floats.Max(e), Y: floats.Max(n)},
		},
	}, nil
}

// Index returns the index of the grid cell containing the coordinates (x,y).
// The coordinates are assumed to be in the same grid projection as the SR.
// If (x,y) is outside of the SR spatial domain, -1 is returned.
func (g *GridCells) Index(x, y float64) int {
	p := geom.Point{X: x, Y: y}
	if !g.Bounds.Overlaps(p.Bounds()) {
		return -1
	}
	neighbor := g.tree.NearestNeighbor(p)
	return neighbor.(cellIndex).index
}

// IndicesWithinRadius returns the indices of grid cells where the centroid
// of the grid cell is within radius r of point (x,y).
// The coordinates are assumed to be in the same grid projection as the SR.
func (g *GridCells) IndicesWithinRadius(x, y, r float64) []int {
	b := geom.Bounds{
		Min: geom.Point{X: x - r, Y: y - r},
		Max: geom.Point{X: x + r, Y: y + r},
	}
	var i []int
	for _, ci := range g.tree.SearchIntersect(&b) {
		cp, err := op.Centroid(ci.(cellIndex).Polygonal)
		if err != nil {
			panic(err)
		}
		d := math.Hypot(x-cp.X, y-cp.Y) // radius
		if d <= r {
			i = append(i, ci.(cellIndex).index)
		}
	}
	return i
}

// CellsIntersecting returns the indices of the cells that intersect gg,
// along with the geometries of the sections of the cells that intersect gg.
// gg is assumed to be in the same grid projection as the SR.
func (g *GridCells) CellsIntersecting(gg geom.Polygonal) ([]int, []geom.Polygonal) {
	var i []int
	var ggg []geom.Polygonal
	for _, ci := range g.tree.SearchIntersect(gg.Bounds()) {
		gIsect := gg.Intersection(ci.(cellIndex).Polygonal)
		if gIsect != nil {
			i = append(i, ci.(cellIndex).index)
			ggg = append(ggg, gIsect)
		}
	}
	return i, ggg
}

// AllocateTotal allocates values associated with geometry gg to the grid
// geometry, preserving the sum of the values. It is useful for example
// to get gridded values for a non-default population.
func (g *GridCells) AllocateTotal(gg []geom.Polygonal, values []float64) ([]float64, error) {
	o := make([]float64, len(g.Cells))
	for i, ggg := range gg {
		for _, cellI := range g.tree.SearchIntersect(ggg.Bounds()) {
			cell := cellI.(cellIndex)
			iSect := ggg.Intersection(cell.Polygonal)
			// Weighted by the fraction of ggg that intersects with cell.
			aFrac := iSect.Area() / ggg.Area()
			o[cell.index] += values[i] * aFrac
		}
	}
	return o, nil
}

// AllocateRate allocates values associated with geometry gg to the grid
// geometry, preserving the area-weighted average of the values. It is useful for example
// to get gridded values for a non-default mortality rate.
func (g *GridCells) AllocateRate(gg []geom.Polygonal, values []float64) ([]float64, error) {
	o := make([]float64, len(g.Cells))
	areas := make([]float64, len(g.Cells))
	for i, ggg := range gg {
		for _, cellI := range g.tree.SearchIntersect(ggg.Bounds()) {
			cell := cellI.(cellIndex)
			iSect := ggg.Intersection(cell.Polygonal)
			// Weighted by this intersection area divided by the total intersection area.
			// This way the average doesn't get dragged down by areas with no value,
			// like over the ocean.
			aIsect := op.Area(iSect)
			areas[cell.index] += aIsect
			o[cell.index] += values[i] * aIsect
		}
	}
	for i, v := range o {
		if v != 0 {
			o[i] = v / areas[i]
		}
	}
	return o, nil
}

// readFullVar reads a full float32 variable and returns it as a
// []float64.
func (sr *SR) readFullVar(varName string) ([]float64, error) {
	r := sr.File.Reader(varName, nil, nil)
	buf := r.Zero(-1)
	_, err := r.Read(buf)
	if err != nil {
		return nil, err
	}
	dat32 := buf.([]float32)
	dat64 := make([]float64, len(dat32))
	for i, v := range dat32 {
		dat64[i] = float64(v)
	}
	return dat64, err
}

// Source returns the changes in concentrations (μg/m3) in every grid cell
// caused by a change of 1 ton/year of emissions of pollutant "pol"
// in grid cell index "index", at height layer "layer".
// pol can be one of "soa", "primarypm2_5", "pnh4", "pso4", or "pno3";
// and layer can be 0, 1, or 2.
func (sr *SR) Source(pol string, index, layer int) ([]float64, error) {
	if index > 8611 && index < 29875 && layer == 0 {
		// Adjust for the change in number of grid cells
		index += 3
	}
	start := []int{layer, index, 0}
	end := []int{layer, index, numCells - 1}
	return sr.get(pol, start, end)
}

// Receptor returns the changes in grid cell index "index"
// caused by a change of 1 ton/year of emissions of pollutant "pol"
// in every grid cell at height layer "layer".
// pol can be one of "soa", "primarypm2_5", "pnh4", "pso4", or "pno3";
// and layer can be 0, 1, or 2. This function has not been tested, so use
// at your own risk.
func (sr *SR) Receptor(pol string, index, layer int) ([]float64, error) {
	start := []int{layer, 0, index}
	end := []int{layer, numCells - 1, index}
	d, err := sr.get(pol, start, end)
	if err != nil {
		return nil, err
	}
	if layer == 0 {
		// Adjust for the change in number of grid cells.
		a := append(d[0:8611], d[8615:22975]...)
		b := append([]float64{0, 0, 0}, d[22978:len(d)]...)
		d = append(a, b...)
	}
	return d, nil
}

// get returns data from a variable starting and ending at the given indices.
func (sr *SR) get(pol string, start, end []int) ([]float64, error) {
	if err := checkPol(pol); err != nil {
		return nil, err
	}
	// indices: layer, source, receptor.
	r := sr.File.Reader(pol, start, end)
	buf := r.Zero(-1)
	_, err := r.Read(buf)
	if err != nil {
		return nil, err
	}
	dat32 := buf.([]float32)
	dat64 := make([]float64, len(dat32))
	for i, v := range dat32 {
		dat64[i] = float64(v)
	}
	return dat64, err
}

func checkPol(pol string) error {
	found := false
	for _, p := range []string{"soa", "primarypm2_5", "pnh4", "pso4", "pno3"} {
		if pol == p {
			found = true
			break
		}
	}
	if !found {
		return fmt.Errorf("unknown pollutant %s", pol)
	}
	return nil
}
