package main

import (
	"database/sql"
	"encoding/csv"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"net/rpc"
	"os"
	"os/exec"
	"path/filepath"
	"time"

	"github.com/ctessum/inmap"
	_ "github.com/mattn/go-sqlite3"
)

var (
	logDir        = "logs"
	inmapData     = "inmapData_2005_v1.1.0/inmapData_[layer].gob"
	numIterations = 10000 // number of iterations for run InMAP for.
)

type Empty struct{}

var RPCport = "6099" // Port for RPC communications for distributed computing

// Program starts here. The environment variable $PBS_NODEFILE
// needs to be set externally (such as by running in a PBS
// environment) for the program to work.
func main() {
	var isSlave *bool = flag.Bool("slave", false, "Is this a slave?")
	flag.Parse()

	if *isSlave {
		// Set up a server to accept RPC requests;
		// this will block and run forever.
		runSlave()
	}

	emisChan := make(chan *IOData)
	outputChan := make(chan *IOData)
	runChan := make(chan int)
	writeFinishedChan := make(chan int)

	// create list of slaves
	slaves := getSlaves()
	for _, s := range slaves {
		go manageSlave(s, emisChan, outputChan, runChan, inmapData,
			numIterations)
	}

	go writeResults(writeFinishedChan, outputChan) // Start process to write results to file

	runMaster(emisChan)

	for i := 0; i < len(slaves); i++ {
		<-runChan
	}
	close(outputChan)
	<-writeFinishedChan
}

// IOData holds the input to and output from the slaves.
type IOData struct {
	Data       map[string][]float64
	Row, Layer int
}

// runMaster sets up emissions for each source location and
// sends them to be processed by the slaves.
func runMaster(emisChan chan *IOData) {
	log.Println("Initializing master")
	d, err := inmap.InitInMAPdata(inmap.UseFileTemplate(inmapData, 27), 10000, "")
	if err != nil {
		panic(err)
	}
	log.Println("Initialized master")
	for _, cell := range d.Data {
		const (
			ft2m     = 0.3048
			diam     = 11.28 * ft2m             // m
			temp     = (377.-32)*5./9. + 273.15 // K
			velocity = 61.94 * ft2m             // m/s
			// Input units = tons/year; output units = μg/s
			massConv = 907184740000.       // μg per short ton
			timeConv = 3600. * 8760.       // seconds per year
			emisConv = massConv / timeConv // convert tons/year to μg/s
		)
		// layers at 0-57, 57-140, and 760-1000 m
		//if (cell.Layer == 0 || cell.Layer == 1 || cell.Layer == 6) &&
		//	cell.Row > 348671 { // Start where we left off.
		emissions := new(IOData)
		emissions.Data = make(map[string][]float64)
		emissions.Row = cell.Row
		emissions.Layer = cell.Layer
		for _, pol := range inmap.EmisNames {
			emissions.Data[pol] = make([]float64, len(d.Data))
			emissions.Data[pol][cell.Row] = 1. * emisConv // 1 ton emissions
		}
		log.Printf("Sending row=%v, layer=%v\n", cell.Row, cell.Layer)
		emisChan <- emissions
		//}
	}
	close(emisChan)
}

func writeResults(runChan chan int, resultChan chan *IOData) {
	var err error
	var outfile = "inmapSR.db"
	//os.Remove(outfile)
	db, err := sql.Open("sqlite3", outfile)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	//_, err = db.Exec("CREATE TABLE sr (id integer not null primary key, " +
	//	"srow integer, layer integer, rrow integer, " +
	//	"SOA real, PrimaryPM2_5 real, pNH4 real, pSO4 real, pNO3 real)")
	if err != nil {
		panic(err)
	}

	for result := range resultChan {
		log.Printf("Writing results for row=%v, layer=%v\n",
			result.Row, result.Layer)
		tx, err := db.Begin()
		if err != nil {
			panic(err)
		}
		newRow, err := tx.Prepare("INSERT INTO sr (srow, layer, rrow, " +
			"SOA, PrimaryPM2_5, pNH4, pSO4, pNO3)" +
			"VALUES (?,?,?,?,?,?,?,?)")
		if err != nil {
			panic(err)
		}
		nRows := len(result.Data["SOA"])
		for i := 0; i < nRows; i++ {
			_, err = newRow.Exec(result.Row, result.Layer, i,
				result.Data["SOA"][i],
				result.Data["PrimaryPM2_5"][i],
				result.Data["pNH4"][i],
				result.Data["pSO4"][i],
				result.Data["pNO3"][i])
			if err != nil {
				panic(err)
			}
		}
		newRow.Close()
		log.Printf("Starting writing results for row=%v, layer=%v\n",
			result.Row, result.Layer)
		err = tx.Commit()
		if err != nil {
			panic(err)
		}
		log.Printf("Finished writing results for row=%v, layer=%v\n",
			result.Row, result.Layer)
	}
	runChan <- 0
}

type Slave struct {
	D *inmap.InMAPdata
}

type InmapInitData struct {
	InmapData     string
	NumIterations int
}

func (s *Slave) Initialize(in *InmapInitData, out *Empty) error {
	log.Println("Initializing slave")
	var err error
	s.D, err = inmap.InitInMAPdata(inmap.UseFileTemplate(in.InmapData, 27), in.NumIterations, "")
	return err
}

func (s *Slave) Calculate(input *IOData,
	output *IOData) error {
	log.Printf("Slave calculating row=%v, layer=%v\n", input.Row, input.Layer)

	o := s.D.Run(input.Data, false)
	output.Data = make(map[string][]float64)
	output.Row = input.Row
	output.Layer = input.Layer
	for pol, data := range o {
		output.Data[pol] = data[0]
	}
	return nil
}

func (s *Slave) Exit(in, out *Empty) error {
	os.Exit(0)
	return nil
}

// runSlave sets up an RPC listener. It is only used when
// the program is running in slave mode.
func runSlave() {
	s := new(Slave)
	rpc.Register(s)
	rpc.HandleHTTP()
	l, err := net.Listen("tcp", ":"+RPCport)
	if err != nil {
		panic(err)
	}
	log.Println("Started slave")
	panic(http.Serve(l, nil))
}

// manageSlave spawns a slave at addr, then feeds it inputs and
// retrieves its outputs through RPC calls.
func manageSlave(addr string, emisChan, outputChan chan *IOData,
	runChan chan int, inmapData string, numIterations int) {
	spawnSlave(addr, logDir)

	client, err := rpc.DialHTTP("tcp", addr+":"+RPCport)
	if err != nil {
		panic(fmt.Sprintf("While dialing %v: %v", addr, err))
	}
	log.Println("Should be initializing slave " + addr)
	init := &InmapInitData{
		InmapData:     inmapData,
		NumIterations: numIterations,
	}
	err = client.Call("Slave.Initialize", init, &Empty{})
	if err != nil {
		panic(err)
	}
	log.Println("Should be finished initializing slave")
	for input := range emisChan {
		output := new(IOData)
		err = client.Call("Slave.Calculate", input, output)
		if err != nil {
			panic(err)
		}
		outputChan <- output
	}
	// kill the slave after we're done with it.
	log.Printf("Killing slave %v", addr)
	client.Call("Slave.Exit", &Empty{}, &Empty{})
	runChan <- 0
}

// spawnSlave executes an external process to spawn a slave
// at the address "addr" using the external "ssh" command.
// Stdout from the slave is routed
// to the directory "logDir".
func spawnSlave(addr, logDir string) *exec.Cmd {
	log.Println("Spawning slave ", addr)
	cmdtxt := fmt.Sprintf("export inmapdata=%s; sr -slave", inmapData)
	cmd := exec.Command("ssh", addr, cmdtxt)

	f, err := os.Create(filepath.Join(logDir, addr+".log"))
	handle(err)
	cmd.Stdout = f
	cmd.Stderr = f

	// After a Session is created, we execute a single command on
	// the remote side using the Run method.
	go func() {
		if err := cmd.Run(); err != nil {
			if err.Error() == "signal: killed" {
				log.Printf("Slave %v expected error: %v", addr, err.Error())
			} else {
				panic(fmt.Errorf("Slave %v error: %v", addr, err.Error()))
			}
		}
	}()
	time.Sleep(time.Second * 10) // wait ten seconds for it to get started
	return cmd
}

// getSlaves read the contents of $PBS_NODEFILE and returns a list of
// unique nodes.
func getSlaves() []string {
	fname := os.ExpandEnv("$PBS_NODEFILE")
	if fname == "" {
		panic("$PBS_NODEFILE not defined")
	}
	f, err := os.Open(fname)
	handle(err)
	r := csv.NewReader(f)
	lines, err := r.ReadAll()
	handle(err)
	//master := lines[0][0]
	slavesMap := make(map[string]Empty)
	for _, l := range lines {
		//if l[0] != master {
		slavesMap[l[0]] = Empty{}
		//}
	}
	var slaves []string
	for s := range slavesMap {
		slaves = append(slaves, s)
	}
	return slaves
}

func handle(err error) {
	if err != nil {
		panic(err)
	}
}
