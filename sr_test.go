package main

import (
	"fmt"
	"log"
	"math"
	"runtime"
	"testing"
	"time"

	"github.com/ctessum/inmap"
)

func BenchmarkDistributed(t *testing.B) {
	numIterations = 100 // number of iterations for run InMAP for

	// numNodes is a list of the different numbers of nodes to try.
	// All of the numbers must be <= the number of nodes available.
	numNodesToTry := []int{1, 2, 4} //10, 40}

	var timing []time.Duration
	for _, numNodes := range numNodesToTry {
		log.Printf("Running test with %v nodes", numNodes)
		// Set up local communication channels
		emisChan := make(chan *IOData)
		outputChan := make(chan *IOData)
		runChan := make(chan int)
		writeFinishedChan := make(chan int)

		// create list of available nodes to spawn slaves on
		slaves := getSlaves()

		// spawn slaves on the desired number of nodes
		for j, s := range slaves {
			if j < numNodes {
				go manageSlave(s, emisChan, outputChan, runChan,
					inmapData, numIterations)
			}
		}

		// Start a local process to handle the results from the slaves.
		// In the real program, these results would be written to a
		// database.
		go handleResults(writeFinishedChan, outputChan)

		start := time.Now()
		// run the master process
		runMasterBenchmark(emisChan, numNodes)

		// After the master finishes running, wait for all of the slaves
		// to finish processing the queued tasks.
		for i := 0; i < numNodes; i++ {
			<-runChan
		}
		// After the slaves are finished, wait for the results handler
		// to finish
		close(outputChan)
		<-writeFinishedChan
		timing = append(timing, time.Since(start))
		log.Printf("Finished test with %v nodes", numNodes)
	}
	timePerOp0 := timing[0].Seconds() / float64(numNodesToTry[0]*opsPerNode)
	for i, numNodes := range numNodesToTry {
		timePerOp := timing[i].Seconds() / float64(numNodes*opsPerNode)
		fmt.Printf("For %v nodes\ttime per op = %v s\tscale eff = %.3g\n",
			numNodes, timePerOp, timePerOp0/(timePerOp*float64(numNodes)))
	}
}

// Handle results deals with the results received from the slaves.
// Currently it just logs that the results were received.
func handleResults(runChan chan int, resultChan chan *IOData) {

	for result := range resultChan {
		log.Printf("Received results for row=%v, layer=%v\n",
			result.Row, result.Layer)
	}
	runChan <- 0
}

const opsPerNode = 3

// runMasterBenchmark sets up emissions for each source location and
// sends them to be processed by the slaves. It is a version
// of runMaster altered for benchmarking. It is set up to
// run 3*numNodes simulations with 100 iterations each (the real runMaster is
// set up to run ~150,000 simulations with 10,000 iterations each).
func runMasterBenchmark(emisChan chan *IOData, numNodes int) {
	log.Println("Initializing master")
	d, err := inmap.InitInMAPdata(inmap.UseFileTemplate(inmapData, 27), 10, "")
	if err != nil {
		panic(err)
	}
	log.Println("Initialized master")
	for _, cell := range d.Data[0 : numNodes*opsPerNode] {
		emissions := new(IOData)
		emissions.Data = make(map[string][]float64)
		emissions.Row = cell.Row
		emissions.Layer = cell.Layer
		for _, pol := range inmap.EmisNames {
			emissions.Data[pol] = make([]float64, len(d.Data))
			emissions.Data[pol][cell.Row] = 1. // arbitrary emissions amount
		}
		log.Printf("Sending row=%v, layer=%v\n", cell.Row, cell.Layer)
		emisChan <- emissions
	}
	close(emisChan)
}

func BenchmarkRunInMAP(b *testing.B) {

	const (
		Δt               = 6.  // seconds
		numRunIterations = 100 // number of iterations for Run to run
	)

	d, err := inmap.InitInMAPdata(
		inmap.UseFileTemplate(inmapData, 27), numRunIterations, "")
	if err != nil {
		panic(err)
	}
	d.Dt = Δt

	var timing []time.Duration
	var procs = []int{1, 2, 4, 8, 16, 24, 36, 48}
	for _, nprocs := range procs {
		runtime.GOMAXPROCS(nprocs)
		emissions := make(map[string][]float64)
		emissions["SOx"] = make([]float64, len(d.Data))
		emissions["SOx"][25000] = 100.
		var results []float64
		start := time.Now()
		results = d.Run(emissions, false)["TotalPM2_5"][0]
		timing = append(timing, time.Since(start))
		totald := 0.
		for _, v := range results {
			totald += v
		}
		const expectedSum = 7.191501683235596e-10
		if different(totald+1, expectedSum+1) {
			b.Errorf("Deaths (%v) doesn't equal %v", totald, expectedSum)
		}
	}
	for i, p := range procs {
		fmt.Printf("For %v procs\ttime = %v\tscale eff = %.3g\n",
			p, timing[i], timing[0].Seconds()/
				timing[i].Seconds()/float64(p))
	}
}

func different(a, b float64) bool {
	const testTolerance = 1e-4

	if 2*math.Abs(a-b)/math.Abs(a+b) > testTolerance {
		return true
	}
	return false
}
