package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"strconv"

	"bitbucket.org/ctessum/cdf"
	"github.com/ctessum/geom/encoding/shp"
	"github.com/gonum/floats"
	_ "github.com/mattn/go-sqlite3"
)

const (
	layers                                   = 3
	gridCells                                = 50105
	gridCellsExtra                           = 50108
	iSOA, iPrimaryPM2_5, ipNH4, iPSO4, iPNO3 = 0, 1, 2, 3, 4
	inmapData                                = "/home/marshall/tessumcm/inmapData/inmapData_48_24_12_4_2_1_40000_old/inmapData_0.shp"
)

func main() {

	log.Println("allocating array")
	// indexes: var, layer, source, receptor
	sr := make([][][][]float64, 5)
	for k := range sr {
		sr[k] = make([][][]float64, layers)
		for i := range sr[k] {
			sr[k][i] = make([][]float64, gridCells)
			for j := range sr[k][i] {
				sr[k][i][j] = make([]float64, gridCells, gridCellsExtra)
			}
		}
	}

	var outfile = "../inmapSR.db"
	db, err := sql.Open("sqlite3", outfile)
	handle(err)
	defer db.Close()

	log.Println("Querying db")
	rows, err := db.Query("SELECT srow, layer, rrow, SOA, PrimaryPM2_5, " +
		"pNH4, pSO4, pNO3 FROM sr") // LIMIT 2000000")
	handle(err)
	defer rows.Close()
	oldrow := -1
	for rows.Next() {
		var SRow, Layer, RRow int
		var SOA, PrimaryPM2_5, PNH4, PSO4, PNO3 float64
		handle(rows.Scan(&SRow, &Layer, &RRow, &SOA, &PrimaryPM2_5,
			&PNH4, &PSO4, &PNO3))

		if oldrow != SRow {
			oldrow = SRow
			log.Printf("Running layer %d row %d\n", Layer, SRow)
		}

		SRow -= gridCells * Layer

		// Adjust layer to be contiguous
		if Layer == 6 {
			Layer = 2
		}

		if RRow >= gridCells && len(sr[iSOA][Layer][SRow]) == gridCells {
			for ip := 0; ip < 5; ip++ {
				sr[ip][Layer][SRow] = append(sr[ip][Layer][SRow], 0, 0, 0)
			}
		}
		for ip, val := range []float64{SOA, PrimaryPM2_5, PNH4, PSO4, PNO3} {
			sr[ip][Layer][SRow][RRow] = val
		}
	}
	handle(rows.Err())
	maxExtraCells := 0

	for ip, v := range sr {
		for Layer, vv := range v {
			for srow, vvv := range vv {
				numReceptors := len(vvv)
				if numReceptors > gridCells {
					if numReceptors != gridCellsExtra {
						panic(fmt.Errorf("invalid number of cells %v",
							numReceptors))
					}
					if srow > maxExtraCells {
						maxExtraCells = srow
					}
					// average the 4 cells that were added
					vvv[8611] = (vvv[8611] + vvv[8612] + vvv[8613] +
						vvv[8614]) / 4
					// delete the remaining 3 cells
					sr[ip][Layer][srow] = append(vvv[0:8612], vvv[8615:len(vvv)]...)

				}
				if floats.Sum(vvv) == 0 {
					fmt.Printf("source row %d layer %d missing\n",
						srow, Layer)
				}
			}
		}
	}
	fmt.Println("Max extra cells: ", maxExtraCells)

	writeNCF(sr)
}

func writeNCF(sr [][][][]float64) {
	log.Println("writing output")
	h := cdf.NewHeader([]string{"layer", "source", "receptor"},
		[]int{layers, gridCells, gridCells})

	ncvars1 := []string{"soa", "primarypm2_5", "pnh4", "pso4", "pno3"}
	for _, vs := range ncvars1 {
		h.AddVariable(vs, []string{"layer", "source", "receptor"},
			[]float32{0})
	}
	ncvars2 := []string{"N", "S", "E", "W", "TotalPop", "WhiteNoLat",
		"Black", "Native", "Asian", "Other", "Latino",
		"Poverty", "TwoXPov", "AllCause"}
	for _, vs := range ncvars2 {
		h.AddVariable(vs, []string{"source"}, []float32{0})
	}
	h.Define()

	ff, err := os.Create("sr.ncf")
	handle(err)
	defer ff.Close()
	f, err := cdf.Create(ff, h)
	handle(err)

	for ip, vs := range ncvars1 {
		log.Println("writing ", vs)
		dd := make([]float32, layers*gridCells*gridCells)
		ii := 0
		for _, v := range sr[ip] {
			for _, vv := range v {
				if len(vv) != gridCells {
					panic(fmt.Errorf("incorrect number of grid cells %d", len(vv)))
				}
				for _, vvv := range vv {
					dd[ii] = float32(vvv)
					ii++
				}
			}
		}
		w := f.Writer(vs, []int{0, 0, 0},
			[]int{layers, gridCells, gridCells})
		_, err := w.Write(dd)
		handle(err)
	}
	d := make([][]float32, len(ncvars2))
	for i := range d {
		d[i] = make([]float32, gridCells)
	}
	dec, err := shp.NewDecoder(inmapData)
	handle(err)
	i := 0
	for {
		g, fields, more := dec.DecodeRowFields(ncvars2[4:len(ncvars2)]...)
		if !more {
			break
		}
		b := g.Bounds(nil)
		d[0][i] = float32(b.Max.Y)
		d[1][i] = float32(b.Min.Y)
		d[2][i] = float32(b.Max.X)
		d[3][i] = float32(b.Min.X)
		d[4][i] = float32(s2f(fields["TotalPop"]))
		d[5][i] = float32(s2f(fields["WhiteNoLat"]))
		d[6][i] = float32(s2f(fields["Black"]))
		d[7][i] = float32(s2f(fields["Native"]))
		d[8][i] = float32(s2f(fields["Asian"]))
		d[9][i] = float32(s2f(fields["Other"]))
		d[10][i] = float32(s2f(fields["Latino"]))
		d[11][i] = float32(s2f(fields["Poverty"]))
		d[12][i] = float32(s2f(fields["TwoXPov"]))
		d[13][i] = float32(s2f(fields["AllCause"]))
		i++
	}
	handle(dec.Error())
	for i, vs := range ncvars2 {
		w := f.Writer(vs, []int{0}, []int{gridCells})
		_, err := w.Write(d[i])
		handle(err)
	}
}

func handle(err error) {
	if err != nil {
		panic(err)
	}
}

func s2f(s string) float64 {
	f, err := strconv.ParseFloat(s, 64)
	handle(err)
	return f
}
